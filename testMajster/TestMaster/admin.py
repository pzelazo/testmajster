# TestMajster/TestMaster/admin.py

from django.contrib import admin
from .models import TestCase, TestStep

admin.site.register(TestCase)
admin.site.register(TestStep)
