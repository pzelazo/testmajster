# TestMaster/views.py
import form
from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse

from .forms import TestCaseForm
from .models import TestCase


def index(request):
    test_cases = TestCase.objects.all()  # Get all test cases from the database
    return render(request, 'TestMaster/index.html', {'test_cases': test_cases})


def new_test_case(request):
    if request.method == 'POST':
        form = TestCaseForm(request.POST)
        if form.is_valid():
            test_case = form.save()
            return redirect('TestMaster:test_cases_list')
    else:
        form = TestCaseForm()
    return render(request, 'TestMaster/new_test_case.html', {'form': form})


def test_cases_list(request):
    test_cases = TestCase.objects.all().order_by('id')
    return render(request, 'TestMaster/test_cases_list.html', {'test_cases': test_cases})


def test_case_detail(request, pk):
    test_case = get_object_or_404(TestCase, pk=pk)
    test_steps = test_case.test_steps.all()  # Use the correct related_name
    return render(request, 'TestMaster/test_case_detail.html', {'test_case': test_case, 'test_steps': test_steps})


def test_case_edit(request, pk):
    test_case = get_object_or_404(TestCase, pk=pk)
    if request.method == 'POST':
        form = TestCaseForm(request.POST, instance=test_case)
        if form.is_valid():
            form.save()
            return redirect('TestMaster:test-cases-list')
    else:
        form = TestCaseForm(instance=test_case)

    context = {'form': form}
    return render(request, 'TestMaster/test_case_edit.html', context)
