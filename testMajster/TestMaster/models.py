from django.db import models


# Define the TestCase model
class TestCase(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    # Assume you have fields for status and expected_result here as well
    status = models.CharField(max_length=50, choices=[('Open', 'Open'), ('Closed', 'Closed'), ('In Progress', 'In Progress')])
    expected_result = models.TextField()

    def __str__(self):
        return self.title


# Define the TestStep model which relates to TestCase
class TestStep(models.Model):
    test_case = models.ForeignKey(TestCase, related_name='test_steps', on_delete=models.CASCADE)
    step_description = models.TextField()
    expected_result = models.TextField()

    def __str__(self):
        return f"Step for {self.test_case.title}: {self.step_description}"

# Remember to add 'TestMaster' to the INSTALLED_APPS setting in settings.py
# After updating your models
