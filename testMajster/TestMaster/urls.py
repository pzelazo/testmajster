# TestMajster/TestMaster/urls.py

from django.urls import path
from . import views
from .views import test_cases_list

app_name = 'TestMaster'

urlpatterns = [
    path('', views.index, name='index'),  # The root path for listing test cases
    path('new/', views.new_test_case, name='new_test_case'),  # The path for the new test case form
    path('test-cases/', views.test_cases_list, name='test-cases-list'),
    # path('test-case/detail/<int:pk>/', views.test_case_detail, name='test-case-detail'),
    path('test-case/edit/<int:pk>/', views.test_case_edit, name='test-case-edit'),
]
