from django import forms
from django.forms import inlineformset_factory
from .models import TestCase, TestStep


class TestCaseForm(forms.ModelForm):
    class Meta:
        model = TestCase
        fields = ['title', 'description', 'status', 'expected_result']


# Define the form for individual test steps
class TestStepForm(forms.ModelForm):
    class Meta:
        model = TestStep
        fields = ['step_description', 'expected_result']  # Assuming these are the fields on your TestStep model


# Create a formset for test steps related to a test case
TestStepFormSet = inlineformset_factory(
    parent_model=TestCase,
    model=TestStep,
    form=TestStepForm,
    extra=1,
    can_delete=True
)
