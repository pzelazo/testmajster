from django.apps import AppConfig


class TestmasterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TestMaster'
